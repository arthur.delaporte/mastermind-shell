# Mastermind en Shell

Ce projet propose différentes versions du jeu Mastermind en Shell. Chaque version offre une expérience de jeu unique pour les joueurs.

***

## Version 1: 1 contre 1 sur le même ordinateur

Dans cette version, deux joueurs peuvent s'affronter en jouant sur le même ordinateur. Chaque joueur tour à tour devra deviner la combinaison secrète de l'autre.

Pour lancer cette version, exécutez le fichier `Mastermind_1vs1.sh` dans le terminal du dossier.

***

## Version 2: 1 contre l'ordinateur

Dans cette version, un joueur affrontera l'ordinateur. Le joueur devra deviner la combinaison secrète générée par l'ordinateur.

Pour lancer cette version, exécutez le fichier `Mastermind_vsOrdi.sh` dans le terminal du dossier.

***

## Version 3: 1 contre 1 ou 1 contre l'ordinateur

Cette version offre au joueur la possibilité de choisir entre jouer contre un autre joueur sur le même ordinateur ou affronter l'ordinateur.

Pour lancer cette version, exécutez le fichier `Mastermind_1vs1_vsOrdi.sh` dans le terminal du dossier.

***

## Version 4: 1 contre 1 sur deux ordinateurs différents (Réseau)

Dans cette version, deux joueurs peuvent jouer l'un contre l'autre, mais cette fois sur deux ordinateurs différents, en utilisant une connexion réseau.

Pour lancer cette version, exécutez le fichier `Mastermind_Reseau_1vs1.sh` dans le terminal du dossier.

***

## Comment jouer

- Chaque joueur devra entrer sa combinaison de couleurs secrète.
- L'ordinateur ou l'autre joueur devra deviner la combinaison en proposant une série de couleurs.
- Le joueur obtiendra des indications sur la justesse de ses propositions.
- Le jeu se poursuit jusqu'à ce que la combinaison soit correctement devinée ou jusqu'à ce qu'un nombre maximal de tentatives soit atteint.

***

## Installation

Pour jouer au Mastermind en Shell, suivez ces étapes :

1. Clonez le dépôt du projet sur votre ordinateur :

   ```shell
   git clone https://gitlab.com/arthur.delaporte/mastermind-shell.git
   ```

2. Accédez au répertoire du jeu :

   ```shell
   cd mastermind-shell
   ```
3. Assurez-vous d'avoir les permissions nécessaires pour exécuter les fichiers Shell.

4. Lancez le jeu en exécutant l'un des fichiers correspondant à la version de votre choix.

***

## Auteur

Ce projet a été créé par [Arthur Delaporte](https://gitlab.com/arthur.delaporte).
