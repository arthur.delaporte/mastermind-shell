#!/bin/bash

verifChaineOk () {

  #Instanciation de la variable chaine
  chaine=$1
  serveurJoueur=$2

  error=""

  #Test chaine contient que 4 lettres
  if [[ "${#chaine}" == 4 ]]
  then
    #Instanciation de la variable contenant la première lettre de chaine
    l1Chaine=$(echo $chaine | cut -c1)

    #Instanciation de la variable contenant la deuxième lettre de chaine
    l2Chaine=$(echo $chaine | cut -c2)

    #Instanciation de la variable contenant la troisième lettre de chaine
    l3Chaine=$(echo $chaine | cut -c3)

    #Instanciation de la variable contenant la quatrième lettre de chaine
    l4Chaine=$(echo $chaine | cut -c4)

    #
    if [[ $l1Chaine != *[ABCDEFGH]* ]] || [[ $l2Chaine != *[ABCDEFGH]* ]] || [[ $l3Chaine != *[ABCDEFGH]* ]] || [[ $l4Chaine != *[ABCDEFGH]* ]]
    then
      if [ "$serveurJoueur" == "Serveur" ]
      then
        echo "La suite à deviner n'est pas valide"
      else
        msgError="La dernière proposition ne contient pas les bons caractères. "
        echo "" > CSV/lastProposal.csv
      fi
    else
      if [ "$serveurJoueur" == "Serveur" ]
      then
        echo "La suite à deviner est valide"
        suiteDevinerInvalide="Valide"
        echo $chaine > CSV/Solution.csv
      else
        echo $chaine >> CSV/listeReponses.csv
        msgError=""
      fi
    fi

  #Chaine contient pas 4 lettres
  else
      if [ "$serveurJoueur" == "Serveur" ]
      then
        echo "La suite à deviner n'est pas valide"
      else
        msgError="La dernière proposition ne contient 4 lettres. "
        echo "" > CSV/lastProposal.csv
      fi
  fi

}

recupValeursChaineProposee () {
  proposition=$1
  verifChaineOk $proposition Joueur
  suitePropose=$chaine
  lettresProposition[0]=$l1Chaine
  lettresProposition[1]=$l2Chaine
  lettresProposition[2]=$l3Chaine
  lettresProposition[3]=$l4Chaine
}
recupValeursChaineDeviner () {
  solution=$1
  verifChaineOk $solution Serveur
  suiteDeviner=$chaine
  lettresDeviner[0]=$l1Chaine
  lettresDeviner[1]=$l2Chaine
  lettresDeviner[2]=$l3Chaine
  lettresDeviner[3]=$l4Chaine
}

if ! [ -e CSV/Solution.csv ]
then
  until [ "$suiteDevinerInvalide" == "Valide" ]
  do
    read -p "Veuillez saisir la suite à deviner : " suitedeviner
    recupValeursChaineDeviner $suitedeviner
  done
fi

if [ -e CSV/listeReponses.csv ]
then
  lignesListeReponses=$(wc -l < CSV/listeReponses.csv)
  ligneLastProposal=$(wc -l < CSV/lastProposal.csv)
  nombreEssai=$((ligneLastProposal+lignesListeReponses))
  nombreEssaiRestant=$((10-nombreEssai))
elif ! [ -e CSV/listeReponses.csv ]
then
  if [ -e CSV/lastProposal.csv ]
  then
    nombreEssaiRestant=9
  else
    nombreEssaiRestant=10
  fi
fi

msgSaisirProposition="Veuillez saisir une proposition : "

if [ -e CSV/lastProposal.csv ]
then
  derniereproposition=$(tail -n 1 CSV/lastProposal.csv)
  recupValeursChaineProposee $derniereproposition
  msgLastProposal="Dernière Porposition est $derniereproposition. "

  if [ "$msgError" != "" ]
  then
    nombreEssaiRestant=$((nombreEssaiRestant+1))
  fi

  if [ $nombreEssaiRestant == 1 ]
  then
    msgNombreEssaiRestant="Il reste 1 tour. "
  elif [ $nombreEssaiRestant == 0 ]
  then
    msgNombreEssaiRestant="Fin du jeu."
  else
    msgNombreEssaiRestant="Il reste $nombreEssaiRestant tours. "
  fi

  if [ $nombreEssaiRestant == 0 ]
  then
    msgSaisirProposition=""
    msgAnnonceSolution="La réponse était $derniereproposition."
    rm CSV/lastProposal.csv CSV/listeReponses.csv CSV/Solution.csv
  fi

  while read line
  do
    motPropose=$line
    echo "$motPropose"
    echo $motPropose > CSV/lastProposal.csv
    break
  done < <((echo "$msgLastProposal$msgError$msgNombreEssaiRestant$msgSaisirProposition$msgAnnonceSolution") | nc 192.168.1.34 23456)

else
  while read line
  do
    motPropose=$line
    echo "$motPropose"
    echo $motPropose > CSV/lastProposal.csv
    break
  done < <((echo "$msgNombreEssaiRestant$msgSaisirProposition") | nc 192.168.1.34 23456)
fi

# Fait
# Bonne Réception valeur à deviner et valeurs proposées
# Test si suite à deviner et dernière proposition valides
# Suppression dernière proposition si incorrecte
# Dire nombre tours restants

# A faire
# Comparer valeur à deviner et dernière proposition si correcte