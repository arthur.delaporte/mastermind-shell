#!/bin/bash

# création du dossier CSV
if ! [ -d CSV ]
then
  mkdir "CSV"
fi

# Création du dossier ConfigReseau
if ! [ -d ConfigReseau ]
then
  mkdir "ConfigReseau"
fi

# Récupération de l'adresse IP de l'autre joueur et du port à utiliser
if [ -e ConfigReseau/adresseIP.csv ] && [ -e ConfigReseau/port.csv ]
then
  adresseIP=$(tail -n 1 ConfigReseau/adresseIP.csv)
  port=$(tail -n 1 ConfigReseau/port.csv)

# Demande de saisie de l'adresse IP de l'autre joueur et du port à utiliser
else
  read -p "Veuillez saisir l'adresse IP du joueur adverse : " adresseIP
  read -p "Veuillez saisir le port à utiliser : " port

  echo $adresseIP > ConfigReseau/adresseIP.csv
  echo $port > ConfigReseau/port.csv
fi

# Fonction qui permet de vérifier si une chaine est valide ( 4 lettres et avec que A, B, C, D, E, F, G et H)
verifChaineOk () {

  chaine=$1
  serveurJoueur=$2

  error=""

  # Test chaine contient que 4 lettres
  if [[ "${#chaine}" == 4 ]]
  then

    # Recuperation de chaque lettre
    recupChaqueLettre $chaine

    # Chaine ne contient pas les lettres acceptées
    if [[ $l1Chaine != *[ABCDEFGH]* ]] || [[ $l2Chaine != *[ABCDEFGH]* ]] || [[ $l3Chaine != *[ABCDEFGH]* ]] || [[ $l4Chaine != *[ABCDEFGH]* ]]
    then
      if [ "$serveurJoueur" == "Serveur" ]
      then
        echo "La suite à deviner n'est pas valide"
      else
        msgError="Invalide"
        echo "" > CSV/lastProposal.csv
      fi

    # Chaine est valide
    else
      if [ "$serveurJoueur" == "Serveur" ]
      then
        echo "La suite à deviner est valide"
        suiteDevinerInvalide="Valide"
        echo $chaine > CSV/Solution.csv
      else
        echo $chaine >> CSV/listeReponses.csv
        msgError=""
      fi
    fi

  # Chaine contient pas 4 lettres
  else
      if [ "$serveurJoueur" == "Serveur" ]
      then
        echo "La suite à deviner n'est pas valide"
      else
        msgError="Invalide"
        echo "" > CSV/lastProposal.csv
      fi
  fi

}

# Fonction permet de séparer chaque lettre d'une chaine de 4 lettres
recupChaqueLettre () {

  chaine=$1

  # Instanciation de la variable contenant la première lettre de chaine
  l1Chaine=$(echo $chaine | cut -c1)

  # Instanciation de la variable contenant la deuxième lettre de chaine
  l2Chaine=$(echo $chaine | cut -c2)

  # Instanciation de la variable contenant la troisième lettre de chaine
  l3Chaine=$(echo $chaine | cut -c3)

  # Instanciation de la variable contenant la quatrième lettre de chaine
  l4Chaine=$(echo $chaine | cut -c4)

}

# Fonction de comparaison de la solution et de la proposition
comparaisonChaine () {

  chaineSolution=$1
  chaineProposee=$2

  # Récupération chaque lettre de la solution
  recupChaqueLettre $chaineSolution
  lettresSolution[0]=$l1Chaine
  lettresSolution[1]=$l2Chaine
  lettresSolution[2]=$l3Chaine
  lettresSolution[3]=$l4Chaine

  # Récupération chaque lettre de la proposition
  recupChaqueLettre $chaineProposee
  lettresProposition[0]=$l1Chaine
  lettresProposition[1]=$l2Chaine
  lettresProposition[2]=$l3Chaine
  lettresProposition[3]=$l4Chaine

  for (( i=0; i<4; i++))
  do
    tableauPosition[$i]=$i
  done

  #Boucle 1 de comparaison -> si lettre bien positionné alors V sinon X
  for (( i=0; i<4; i++))
  do
    if [[ ${lettresProposition[$i]} == ${lettresSolution[$i]} ]]
    then
      tableauResultat[$i]="V"
      tableauPosition[$i]=""
    else
     tableauResultat[$i]="X"
    fi
  done

  #Boucle 2 de comparaison -> si pas de V et lettre est présente et pas dejà comparé alors O
  for (( i=0; i<4; i++))
  do
    if [[ "${tableauResultat[$i]}" != "V" ]]
    then
      for (( j=0; j<4; j++))
      do
        if [[ ${tableauPosition[$j]} != "" ]]
        then
          if [[ ${lettresProposition[$i]} == ${lettresSolution[$j]} ]]
          then
            tableauResultat[$i]="O"
            tableauPosition[$j]=""
          fi
        fi
      done
    fi
  done

  # Instanciation du résultat de la comparaison
  msgVOX="${tableauResultat[0]}${tableauResultat[1]}${tableauResultat[2]}${tableauResultat[3]}"

}

# Demande de la chaine à deviner
if ! [ -e CSV/Solution.csv ]
then
  # Boucle jusqu'à solution valide
  until [ "$suiteDevinerInvalide" == "Valide" ]
  do
    # Demande de saisie de la solution
    read -p "Veuillez saisir la suite à deviner : " suitedeviner
    # Vérification si solution valide
    verifChaineOk $suitedeviner Serveur
  done
fi

# Si au moins proposition a déjà été faite
if [ -e CSV/lastProposal.csv ]
then

  # Config du message pour demander une proposition
  msgSaisirProposition="Veuillez saisir une proposition :"

  # Si il y a déjà eu plusieurs propositions
  if [ -e CSV/listeReponses.csv ]
  then
    lignesListeReponses=$(wc -l < CSV/listeReponses.csv)
    ligneLastProposal=$(wc -l < CSV/lastProposal.csv)
    nombreEssai=$((ligneLastProposal+lignesListeReponses))
    nombreEssaiRestant=$((10-nombreEssai))

    # Si il n'y a qu'une proposition
  elif ! [ -e CSV/listeReponses.csv ]
  then
      nombreEssaiRestant=9
  fi

  # Récupération de la dernière proposition
  derniereproposition=$(tail -n 1 CSV/lastProposal.csv)
  # Vérification si dernière proposition est valide
  verifChaineOk $derniereproposition Joueur
  # Config du message pour rappeler la dernière proposition
  msgLastProposal="La dernière proposition est $derniereproposition : "

  # Récupération de la solution
  solution=$(tail -n 1 CSV/Solution.csv)

  # Appel fonction de comparaison de solution et dernière proposition
  comparaisonChaine $solution $derniereproposition

  # Si dernière proposition est invalide alors tour non comptabilisé
  if [ "$msgError" != "" ]
  then
    nombreEssaiRestant=$((nombreEssaiRestant+1))
  fi

  # Config du message pour si 1 tour restant
  if [ $nombreEssaiRestant == 1 ]
  then
    msgNombreEssaiRestant=". Il reste $nombreEssaiRestant tour. "

  # Config du message pour si aucun tour restant et solution pas trouvée
  elif [ $nombreEssaiRestant == 0 ] && [ "$msgVOX" != "VVVV" ]
  then
    msgNombreEssaiRestant=". Fin du jeu. "
    msgSaisirProposition=""
    msgVicDef="C'est perdu. "
    msgAnnonceSolution="La réponse était $derniereproposition."

  # Config du message pour si solution trouvée
  elif [ "$msgVOX" == "VVVV" ]
  then
    msgNombreEssaiRestant=". Fin du jeu. "
    msgSaisirProposition=""
    msgVicDef="C'est gagné. Bravo !"

  # Config du message pour si solution pas trouvée mais encore des tours restants
  else
    msgNombreEssaiRestant=". Il reste $nombreEssaiRestant tours. "
    msgVicDef="Try again. "
  fi

  # Boucle while pour demander une proposition
  while read line
  do

    # Récupération et affichage de la proposition
    motPropose=$line
    echo "$motPropose"

    # Si aucun tour restant et solution pas trouvée alors suppression des fichiers de stockage de valeurs
    if [ $nombreEssaiRestant == 0 ] && [ "$msgVOX" != "VVVV" ]
    then
      rm CSV/lastProposal.csv CSV/listeReponses.csv CSV/Solution.csv ConfigReseau/adresseIP.csv ConfigReseau/port.csv
    # Si solution trouvée alors suppression des fichiers de stockage de valeurs
    elif [ "$msgVOX" == "VVVV" ]
    then
      rm CSV/lastProposal.csv CSV/listeReponses.csv CSV/Solution.csv ConfigReseau/adresseIP.csv ConfigReseau/port.csv
    # Sinon vide contenu du fichier stockage de la dernière proposition et ajout de la toute dernière proposition
    else
      echo $motPropose > CSV/lastProposal.csv
    fi

    # Break pour sortir de la boucle while
    break

  # Message
  done < <((echo "$msgLastProposal$msgVOX$msgError$msgNombreEssaiRestant$msgVicDef$msgSaisirProposition$msgAnnonceSolution") | nc $adresseIP $port)

# Aucune proposition de déjà faite (début de jeu)
else

  # Boucle while pour demander une proposition
  while read line
  do

    # Récupération et affichage de la proposition
    motPropose=$line
    echo "$motPropose"

    # Ajout de la première proposition dans le fichier stockage de la dernière proposition
    echo $motPropose > CSV/lastProposal.csv

    # Break pour sortir de la boucle while
    break

  # Message
  done < <((echo "Il reste 10 tours. Veuillez saisir une première proposition :") | nc $adresseIP $port)
fi