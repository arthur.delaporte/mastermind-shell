#!/bin/bash

read -p "Veuillez saisir le nom du joueur 1 :" J1
read -p "Veuillez saisir le nom du joueur 2 :" J2

verifChaineOk () {

  #Instanciation de la variable chaine
  chaine=""

  #Récupération du premier argument de la fonction
  actualPlayer=$1

  #Test chaine contient que 4 lettres qui doivent être soit A soit B soit C soit D soit E soit F soit G soit H
  until [[ "${#chaine}" == 4 ]] && [[ $l1Chaine == *[ABCDEFGH]* ]] && [[ $l2Chaine == *[ABCDEFGH]* ]] && [[ $l3Chaine == *[ABCDEFGH]* ]] && [[ $l4Chaine == *[ABCDEFGH]* ]]
  do

    if [ $actualPlayer == "Joueur1" ]
    then
      #Demande de la variable chaine à Joueur1
      read -s -p "Saisir 4 lettres en majuscules comprises entre A et H : " chaine
      echo ""
    elif [ $actualPlayer == "Joueur2" ]
    then
      #Demande de la variable chaine à Joueur2
      read -p "Saisir 4 lettres en majuscules comprises entre A et H : " chaine
    fi

    #Instanciation de la variable contenant la première lettre de chaine
    l1Chaine=$(echo $chaine | cut -c1)

    #Instanciation de la variable contenant la deuxième lettre de chaine
    l2Chaine=$(echo $chaine | cut -c2)

    #Instanciation de la variable contenant la troisième lettre de chaine
    l3Chaine=$(echo $chaine | cut -c3)

    #Instanciation de la variable contenant la quatrième lettre de chaine
    l4Chaine=$(echo $chaine | cut -c4)
  done

}

recupValeursChaine1 () {
  verifChaineOk Joueur1
  suiteDeviner=$chaine
  l1SuiteDeviner=$l1Chaine
  l2SuiteDeviner=$l2Chaine
  l3SuiteDeviner=$l3Chaine
  l4SuiteDeviner=$l4Chaine
}

recupValeursChaine2 () {
  verifChaineOk Joueur2
  suitePropose=$chaine
  l1SuitePropose=$l1Chaine
  l2SuitePropose=$l2Chaine
  l3SuitePropose=$l3Chaine
  l4SuitePropose=$l4Chaine
}

verifLettrePresente () {

  echo "Tour de $J1"
  recupValeursChaine1

  echo "Tour de $J2 "
  tourRestant=10
  tourJoue=1
  until [ $tourRestant == 0 ]
  do

    if [ $tourRestant == 1 ]
    then
      echo "Il te reste 1 tour. Concentre-toi !"
    else
      echo "Il te reste $tourRestant tours."
    fi

    recupValeursChaine2

    for (( i=1; i<$tourJoue; i++))
    do
      if [ $suitePropose == ${tableauPorposition[$i]} ]
      then
        if [ $i == 1 ]
        then
          echo "Vous avez déjà essayé $suitePropose lors de votre ${i}er tour"
        else
          echo "Vous avez déjà essayé $suitePropose lors de votre ${i}ème tour"
        fi
        recupValeursChaine2
      fi
    done

    tableauPorposition[$tourJoue]=$suitePropose

    #V
    if [ $l1SuitePropose == $l1SuiteDeviner ]
    then
      l1Good="V"
    elif [ $l1SuitePropose == $l2SuiteDeviner ] || [ $l1SuitePropose == $l3SuiteDeviner ] || [ $l1SuitePropose == $l4SuiteDeviner ]
    then
      l1Good="O"
    else
      l1Good="X"
    fi

    if [ $l2SuitePropose == $l2SuiteDeviner ]
    then
      l2Good="V"
    elif [ $l2SuitePropose == $l1SuiteDeviner ] || [ $l2SuitePropose == $l3SuiteDeviner ] || [ $l2SuitePropose == $l4SuiteDeviner ]
    then
      l2Good="O"
    else
      l2Good="X"
    fi

    if [ $l3SuitePropose == $l3SuiteDeviner ]
    then
      l3Good="V"
    elif [ $l3SuitePropose == $l1SuiteDeviner ] || [ $l3SuitePropose == $l2SuiteDeviner ] || [ $l3SuitePropose == $l4SuiteDeviner ]
    then
      l3Good="O"
    else
      l3Good="X"
    fi

    if [ $l4SuitePropose == $l4SuiteDeviner ]
    then
      l4Good="V"
    elif [ $l4SuitePropose == $l1SuiteDeviner ] || [ $l4SuitePropose == $l2SuiteDeviner ] || [ $l4SuitePropose == $l3SuiteDeviner ]
    then
      l4Good="O"
    else
      l4Good="X"
    fi

    resultatProposition=$l1Good$l2Good$l3Good$l4Good

    if [ $resultatProposition == "VVVV" ]
    then
      echo "Partie terminée : Victoire de $J2"
      tourRestant=0

    elif [ $tourRestant == 1 ] && [ ! $resultatProposition == "VVVV" ]
    then
      echo "Partie terminée : Victoire de $J1"
      echo "La réponse était $suiteDeviner"
      tourRestant=0

    else
      echo $suitePropose
      echo $resultatProposition
      echo "Try again"
      echo "Vos tentatives sont :"
      for (( i=1; i<=$tourJoue; i++))
      do
        echo ${tableauPorposition[$i]}
      done

      tourJoue=$((tourJoue+1))
      tourRestant=$((tourRestant-1))
    fi

  done
}

verifLettrePresente