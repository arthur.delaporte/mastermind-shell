#!/bin/bash

clear

until [[ $ordi1v1 == 1 ]] || [[ $ordi1v1 == 2 ]]
do
  echo "Voulez-vous jouer contre un ordi ou en 1v1 ?"
  read -p "Tapez 1 pour jouer contre un ordi et 2 pour un 1v1 : " ordi1v1
done

if [ $ordi1v1 == 1 ]
then
  read -p "Veuillez saisir le nom du joueur : " JoueurPropose
elif [ $ordi1v1 == 2 ]
then
  read -p "Veuillez saisir le nom du joueur 1 : " JoueurSolution
  read -p "Veuillez saisir le nom du joueur 2 : " JoueurPropose

  while [[ $JoueurSolution == $JoueurPropose ]]
  do
    read -p "Veuillez saisir un nom différent du joueur 1 pour le joueur 2 : " JoueurPropose
  done
fi

lettresPossibles='ABCDEFGH'

verifChaineOk () {

  #Instanciation de la variable chaine
  chaine=""

  #Récupération du premier argument de la fonction
  actualPlayer=$1

  #Test chaine contient que 4 lettres qui doivent être soit A soit B soit C soit D soit E soit F soit G soit H
  until [[ "${#chaine}" == 4 ]] && [[ $l1Chaine == *[ABCDEFGH]* ]] && [[ $l2Chaine == *[ABCDEFGH]* ]] && [[ $l3Chaine == *[ABCDEFGH]* ]] && [[ $l4Chaine == *[ABCDEFGH]* ]]
  do

    if [ $ordi1v1 == 1 ]
    then
      read -p "Saisir 4 lettres en majuscules comprises entre A et H : " chaine
    elif [ $ordi1v1 == 2 ]
    then
      if [ $actualPlayer == "Joueur1" ]
      then
        #Demande de la variable chaine à Joueur1
        read -s -p "Saisir 4 lettres en majuscules comprises entre A et H : " chaine
        echo ""
      elif [ $actualPlayer == "Joueur2" ]
      then
        #Demande de la variable chaine à Joueur2
        read -p "Saisir 4 lettres en majuscules comprises entre A et H : " chaine
      fi
    fi

    #Instanciation de la variable contenant la première lettre de chaine
    l1Chaine=$(echo $chaine | cut -c1)

    #Instanciation de la variable contenant la deuxième lettre de chaine
    l2Chaine=$(echo $chaine | cut -c2)

    #Instanciation de la variable contenant la troisième lettre de chaine
    l3Chaine=$(echo $chaine | cut -c3)

    #Instanciation de la variable contenant la quatrième lettre de chaine
    l4Chaine=$(echo $chaine | cut -c4)
  done

}

generChaineOrdi () {
  #Random chiffre 1 à 8 puis recupere 1 lettre dans lettresPossibles
  nbrCaracteres=$(( $RANDOM % ${#lettresPossibles}))
  l1ChaineOrdi=${lettresPossibles:$nbrCaracteres:1}

  nbrCaracteres=$(( $RANDOM % ${#lettresPossibles}))
  l2ChaineOrdi=${lettresPossibles:$nbrCaracteres:1}

  nbrCaracteres=$(( $RANDOM % ${#lettresPossibles}))
  l3ChaineOrdi=${lettresPossibles:$nbrCaracteres:1}

  nbrCaracteres=$(( $RANDOM % ${#lettresPossibles}))
  l4ChaineOrdi=${lettresPossibles:$nbrCaracteres:1}
}

#Appel function demande chaine et recuperation de la chaine à deviner et ses lettres separement
recupValeursChaineDeviner () {

  if [ $ordi1v1 == 1 ]
  then
    generChaineOrdi
    lettresDeviner[0]=$l1ChaineOrdi
    lettresDeviner[1]=$l2ChaineOrdi
    lettresDeviner[2]=$l3ChaineOrdi
    lettresDeviner[3]=$l4ChaineOrdi
    suiteDeviner=${lettresDeviner[0]}${lettresDeviner[1]}${lettresDeviner[2]}${lettresDeviner[3]}

  elif [ $ordi1v1 == 2 ]
  then
    verifChaineOk Joueur1
    suiteDeviner=$chaine
    lettresDeviner[0]=$l1Chaine
    lettresDeviner[1]=$l2Chaine
    lettresDeviner[2]=$l3Chaine
    lettresDeviner[3]=$l4Chaine
  fi
}

#Appel function demande chaine et recuperation de la chaine proposee et ses lettres separement
recupValeursChaineProposee () {
  verifChaineOk Joueur2
  suitePropose=$chaine
  lettresProposition[0]=$l1Chaine
  lettresProposition[1]=$l2Chaine
  lettresProposition[2]=$l3Chaine
  lettresProposition[3]=$l4Chaine
}

#Verification de lettres presentes et donne le resultat
verifLettresPresentent () {

  #Appel de la fonction recupValeursChaineDeviner
  if [ $ordi1v1 == 2 ]
  then
    echo "Tour de $JoueurSolution"
  fi
  recupValeursChaineDeviner

  echo "Tour de $JoueurPropose"

  tourRestant=10
  tourJoue=1

  #Boucle pour laisser 10 essais au joueur qui devine
  until [ $tourRestant == 0 ]
  do

    #Annonce le nombre de tours restant
    if [ $tourRestant == 1 ]
    then
      echo "Il te reste 1 tour. Concentre-toi !"
    else
      echo "Il te reste $tourRestant tours."
    fi

    #Appel de la fonction recupValeursChainePropose
    recupValeursChaineProposee

    #Boucle pour eviter que la dernière proposition est déjà été donné
    for (( i=1; i<$tourJoue; i++))
    do
      if [ $suitePropose == ${tableauProposition[$i]} ]
      then
        if [ $i == 1 ]
        then
          echo "Vous avez déjà essayé $suitePropose lors de votre ${i}er tour"
        else
          echo "Vous avez déjà essayé $suitePropose lors de votre ${i}ème tour"
        fi
        recupValeursChaineProposee
      fi
    done
    tableauProposition[$tourJoue]=$suitePropose

    for (( i=0; i<4; i++))
    do
      tableauPosition[$i]=$i
    done

    #Boucle 1 de comparaison -> si lettre bien positionné alors V sinon X
    for (( i=0; i<4; i++))
    do
      if [[ ${lettresProposition[$i]} == ${lettresDeviner[$i]} ]]
      then
        tableauResultat[$i]="V"
        tableauPosition[$i]=""
      else
       tableauResultat[$i]="X"
      fi
    done

    #Boucle 2 de comparaison -> si pas de V et lettre est présente et pas dejà comparé alors O
    for (( i=0; i<4; i++))
    do
      if [[ "${tableauResultat[$i]}" != "V" ]]
      then
        for (( j=0; j<4; j++))
        do
          if [[ ${tableauPosition[$j]} != "" ]]
          then
            if [[ ${lettresProposition[$i]} == ${lettresDeviner[$j]} ]]
            then
              tableauResultat[$i]="O"
              tableauPosition[$j]=""
            fi
          fi
        done
      fi
    done

    #Recuperation du resultat en variable
    resultatProposition=${tableauResultat[0]}${tableauResultat[1]}${tableauResultat[2]}${tableauResultat[3]}

    #Affichage resultat
    #Si VVVV alors c'est gagné
    if [ $resultatProposition == "VVVV" ]
    then
      if [ $tourJoue == 1 ]
      then
        echo "Partie terminée : Victoire de $JoueurPropose en $tourJoue tour"
      else
        echo "Partie terminée : Victoire de $JoueurPropose en $tourJoue tours"
      fi
      tourRestant=0

    #Si pas VVVV et déjà 10 tours de joué alors c'est perdu
    elif [ $tourRestant == 1 ] && [ ! $resultatProposition == "VVVV" ]
    then
      if [ $ordi1v1 == 1 ]
      then
        echo "Partie terminée : Victoire de l'Ordi"
      elif [ $ordi1v1 == 2 ]
      then
        echo "Partie terminée : Victoire de $JoueurSolution"
      fi
      echo "La réponse était $suiteDeviner"
      tourRestant=0

    #Sinon ça joue encore
    else
      echo $suitePropose
      echo $resultatProposition
      echo "Try again"
      echo "Vos tentatives sont :"
      for (( i=1; i<=$tourJoue; i++))
      do
        echo ${tableauProposition[$i]}
      done

      tourJoue=$((tourJoue+1))
      tourRestant=$((tourRestant-1))
    fi

  done
}

#Appel de la fonction verifLettresPresentent
verifLettresPresentent