#!/bin/bash
#Vérification si chaine saisie est OK

#Instanciation de la variable chaine
chaine=""

#Test chaine contient que 4 lettres qui doivent être soit A soit B soit C soit D soit E soit F soit G soit H
until [[ "${#chaine}" == 4 ]] && [[ $l1Chaine == *[ABCDEFGH]* ]] && [[ $l2Chaine == *[ABCDEFGH]* ]] && [[ $l3Chaine == *[ABCDEFGH]* ]] && [[ $l4Chaine == *[ABCDEFGH]* ]]
do

  #Demande de la variable chaine
  read -p "Saisir 4 lettres en majuscules comprises entre A et H : " chaine

  #Instanciation de la variable contenant la première lettre de chaine
  l1Chaine=$(echo $chaine | cut -c1)

  #Instanciation de la variable contenant la deuxième lettre de chaine
  l2Chaine=$(echo $chaine | cut -c2)

  #Instanciation de la variable contenant la troisième lettre de chaine
  l3Chaine=$(echo $chaine | cut -c3)

  #Instanciation de la variable contenant la quatrième lettre de chaine
  l4Chaine=$(echo $chaine | cut -c4)
done

echo $l1Chaine
echo $l2Chaine
echo $l3Chaine
echo $l4Chaine